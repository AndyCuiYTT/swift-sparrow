import { defineComponent, h } from 'vue'

export default defineComponent({
  name: 'rc-image',
  props: {
    url: {
      type: String
    }
  },

  render() {
    return h('img', { src: this.url }, '')
  }
})
